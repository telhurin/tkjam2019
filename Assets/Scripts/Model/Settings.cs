﻿using UnityEngine;

namespace Eremite.Model
{
    [CreateAssetMenu(menuName = "Eremite/Setttings")]
    public class Settings : SO
    {
        public float BaseHealth;
        public float BaseAttack;
        public float AttackWinMultiplayer;
        public float DrawAttackMultiplayer;

        public float AttacksClashTime;
        public float AttacksTravelTime;

        public float TimeToPick;
        public float TimeBetweenTurns;
    }
}
