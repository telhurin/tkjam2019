﻿namespace Eremite.Model
{
    public enum Element
    {
        None,
        Fire,
        Water,
        Nature
    }
}
