﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Eremite.Model
{
    public class GameState
    {
        public List<TurnState> turns = new List<TurnState> { new TurnState() };
        public int draws;

        public TurnState GetLastTurn()
        {
            return turns.Last();
        }
    }

    public class TurnState
    {
        public readonly float startTime;
        public readonly Dictionary<Side, Element> picks = new Dictionary<Side, Element>();

        public TurnState()
        {
            startTime = Time.time;
        }
    }
}
