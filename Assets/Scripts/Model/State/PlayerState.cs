﻿using System;

namespace Eremite.Model
{
    public class PlayerState
    {
        public float hp;
        public float waterAttack;
        public float fireAttack;
        public float natureAttack;
        public Side side;

        public float GetDamage(Element element)
        {
            switch (element)
            {
                case Element.Fire:
                    return fireAttack;
                case Element.Water:
                    return waterAttack;
                case Element.Nature:
                    return natureAttack;
                default:
                    throw new NotImplementedException(element.ToString());
            }
        }

        public void AddDamage(Element element, float multiplayer)
        {
            switch (element)
            {
                case Element.Fire:
                    fireAttack *= multiplayer;
                    break;
                case Element.Water:
                    waterAttack *= multiplayer;
                    break;
                case Element.Nature:
                    natureAttack *= multiplayer;
                    break;
                default:
                    throw new NotImplementedException(element.ToString());
            }
        }
    }

    public enum Side
    {
        None,
        Left,
        Right
    }

    public static class SideExtension
    {
        public static Side Opposite(this Side side)
        {
            if (side == Side.None)
                return Side.None;

            return side == Side.Left ? Side.Right : Side.Left;
        }
    }
}