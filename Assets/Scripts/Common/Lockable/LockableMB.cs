﻿using System.Collections.Generic;

namespace Eremite.View
{
    public interface ILockable
    {
        void Lock(object principal);
        void Release(object principal);
        void SwitchLock(object principal, bool locked);
    }

    public abstract class LockableMB : MB, ILockable
    {
        private readonly List<object> locks = new List<object>();

        public void Lock(object principal)
        {
            if (locks.Contains(principal))
                return;

            LogInfo($"Lock {principal}");
            locks.Add(principal);
            ValidateLock();
        }

        public void Release(object principal)
        {
            LogInfo($"Release {principal}");
            locks.Remove(principal);
            ValidateLock();
        }

        public void SwitchLock(object principal, bool shouldLock)
        {
            if (shouldLock)
                Lock(principal);
            else
                Release(principal);
        }

        protected bool IsLocked()
        {
            return locks.Count > 0;
        }

        protected void ResetLock()
        {
            locks.Clear();
        }

        protected abstract void ValidateLock();
    }
}
