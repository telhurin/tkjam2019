﻿using System;
using UnityEngine;

public static class Log
{
    private static bool Enabled { get { return Application.isEditor; } }
    private static bool ExternalEnabled { get { return true; } }

    public static void Info(object obj)
    {
        if (!Enabled)
            return;

        if(ExternalEnabled)
            Debug.Log(obj);

        EGLogger.Info(obj);
    }

    public static void Warning(object obj)
    {
        if (!Enabled)
            return;

        if (ExternalEnabled)
            Debug.LogWarning(obj);

        EGLogger.Warning(obj);
    }

    public static void Error(object obj)
    {
        if (!Enabled)
            return;

        if (ExternalEnabled)
            Debug.LogError(obj);

        EGLogger.Error(obj);
    }

    public static void Exception(Exception exc)
    {
        if (!Enabled)
            return;

        if (ExternalEnabled)
            Debug.LogException(exc);

        EGLogger.Exception(exc);
    }
}
