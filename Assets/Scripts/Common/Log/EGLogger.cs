﻿using System;
using System.Text;
using UnityEngine;

public static class EGLogger
{
    public static Action OnError { get; set; }

    private const int MaxCapacity = 1010000;
    private const int ClearCapacity = 1000000;

    private static StringBuilder builder = new StringBuilder(MaxCapacity);

    public static void Info(object obj)
    {
        CheckBuilderSize();
        builder.AppendFormat("[L] {0} {1} \n", Time.time, obj);
    }

    public static void Warning(object obj)
    {
        CheckBuilderSize();
        builder.AppendFormat("[W] {0} {1} \n", Time.time, obj);
    }

    public static void Error(object obj)
    {
        CheckBuilderSize();
        builder.AppendFormat("[Er] {0} {1} \n", Time.time, obj);
        OnError?.Invoke();
    }

    public static void Exception(Exception exc)
    {
        CheckBuilderSize();
        builder.AppendFormat("[Ex] {0} {1} \n", Time.time, exc);
        OnError?.Invoke();
    }

    private static void CheckBuilderSize()
    {
        if (builder.Length > ClearCapacity)
            builder.Length = 0;
    }

    public static void Send()
    {
        AddSystemInfo();
    }

    private static string GetName()
    {
        return DateTime.UtcNow.ToString("dd_MMM_HH_mm_ss") + ".txt";
    }

    private static void AddSystemInfo()
    {
        builder.AppendLine();
        builder.Append("# version: " + Application.version);
        builder.AppendLine("# date: " + DateTime.Now.ToString("yy-MM-dd HH:mm:ss:ffff"));
        builder.AppendLine("# model: " + SystemInfo.deviceModel);
        builder.AppendLine("# system: " + SystemInfo.operatingSystem);
    }
}