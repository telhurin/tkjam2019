﻿using Eremite.Controller;
using Eremite.Services;
using Eremite.View;
using System;
using UnityEngine;

namespace Eremite
{
    public class SO : ScriptableObject
    {
        #region Shortcuts

        public IController Controller { get { return MainController.Instance; } }

        protected IRXService RXService { get { return Controller.Services.RXService; } }
        protected ITimeService TimeService { get { return Controller.Services.TimeService; } }
        protected SoundsManager SoundsManager { get { return Controller.SoundsManager; } }

        #endregion

        #region Logging

        [SerializeField]
        private bool debugLogs = false;

        protected void LogInfo(object obj)
        {
            if (debugLogs)
                Log.Info(obj);
        }

        protected void LogWarning(object obj)
        {
            if (debugLogs)
                Log.Warning(obj);
        }

        protected void LogError(object obj)
        {
            if (debugLogs)
                Log.Error(obj);
        }

        protected void LogException(Exception exc)
        {
            if (debugLogs)
                Log.Exception(exc);
        }

        #endregion
    }
}