﻿using Eremite.Controller;
using Eremite.Model;
using Eremite.Services;
using System;
using UnityEngine;

namespace Eremite.View
{
    public class MB : MonoBehaviour, IMB
    {
        #region Shortcuts

        public IController Controller { get { return MainController.Instance; } }

        protected Settings Settings { get { return Controller.Settings; } }
        protected SoundReferences Sounds { get { return Controller.Sounds; } }
        protected IRXService RXService { get { return Controller.Services.RXService; } }
        protected ITimeService TimeService { get { return Controller.Services.TimeService; } }
        protected PlayersService PlayersService { get { return Controller.Services.PlayersService; } }
        protected GameService GameService { get { return Controller.Services.GameService; } }
        protected FloatingTextsService FloatingTextsService { get { return Controller.Services.FloatingTextsService; } }
        protected SpellsService SpellsService { get { return Controller.Services.SpellsService; } }

        #endregion

        protected T GetComponentInChild<T>(string path)
        {
            var child = FindChild(path);
            var component = child.GetComponent<T>();
            if (component == null)
                Log.Error("No component: " + typeof(T).ToString() + " in child: " + path);

            return component;
        }

        public GameObject FindChild(string path)
        {
            var child = transform.Find(path);
            if (child == null)
                Log.Error("No child in path: " + path);

            return child.gameObject;
        }

        #region MonoBehavior wrap

        public bool Destroyed { get { return this == null; } }
        public bool ActiveInHierarchy { get { return gameObject.activeInHierarchy; } }
        public Vector3 Position { get { return transform.position; } }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        #endregion

        #region Logging

        [SerializeField]
        private bool debugLogs = false;

        protected void LogInfo(object obj)
        {
            if (debugLogs)
                Log.Info(obj);
        }

        protected void LogWarning(object obj)
        {
            if (debugLogs)
                Log.Warning(obj);
        }

        protected void LogError(object obj)
        {
            if (debugLogs)
                Log.Error(obj);
        }

        protected void LogException(Exception exc)
        {
            if (debugLogs)
                Log.Exception(exc);
        }

        #endregion

        #region ReferencesHelper

        protected T EnsureComponent<T>(T currentValue, string path = null, int parentsUp = 0) where T : Component
        {
            if (currentValue != null)
                return currentValue;

            T c = FindComponent<T>(path, parentsUp);

            if (c == null)
                Debug.LogWarning($"Can't ensure component for {gameObject.name} of type {typeof(T).Name}");

            return c;
        }

        private Transform GetParent(int parentsUp)
        {
            Transform toReturn = transform;
            for (int i = 0; i < parentsUp; i++)
                toReturn = toReturn.parent;

            return toReturn;
        }

        private T FindComponent<T>(string path, int parentsUp) where T : Component
        {
            T result = null;

            try
            {
                var parent = GetParent(parentsUp);
                var go = path == null ? parent.gameObject : parent.Find(path).gameObject;
                result = go.GetComponent<T>();
            }
            catch(Exception e)
            {
                Debug.LogWarning(e);
            }

            return result;
        }

        #endregion
    }
}