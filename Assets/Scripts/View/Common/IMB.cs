﻿namespace Eremite.View
{
    public interface IMB
    {
        bool ActiveInHierarchy { get; }

        void Destroy();
        void SetActive(bool active);
    }
}
