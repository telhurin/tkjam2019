﻿using Eremite.Model;
using System;
using UnityEngine;
using UniRx;

namespace Eremite.View
{
    public class SpellView : MB
    {
        [SerializeField] Animator animator;
        [SerializeField] EasingFunc easing = EasingFunc.Linear;

        private float startTime;
        private float journeyLength;
        private IDisposable updateRoutine;

        private Vector2 from;
        private Vector2 to;

        public void SetUp(Side side, Vector2 from, Vector2 to)
        {
            SetUpRotation(side);
            SetUpJourney(from, to);
            SetUpRoutine();
            UpdatePosition(0);
            SetActive(true);
        }

        private void SetUpRotation(Side side)
        {
            if (side == Side.Right)
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }

        private void SetUpRoutine()
        {
            updateRoutine = RXService.EveryUpdate().Subscribe(UpdatePosition);
        }

        private void SetUpJourney(Vector2 from, Vector2 to)
        {
            this.from = from;
            this.to = to;
            startTime = Time.time;
            journeyLength = Settings.AttacksTravelTime;
        }

        private void UpdatePosition(long delta)
        {
            float fracJourney = Mathf.Clamp01((Time.time - startTime) / journeyLength);

            if (Mathf.Approximately(fracJourney, 1))
            {
                Remove();
                return;
            }

            transform.position = Vector3.Lerp(from, to, Easing.GetEasingFunc(easing)(fracJourney));
        }

        public void Remove()
        {
            updateRoutine.Dispose();
            SetActive(false);
            ShowDestroy();
        }

        private void ShowDestroy()
        {
            animator.SetTrigger("Destroy");
            Destroy(gameObject, 2f);
        }

        private void OnValidate()
        {
            animator = EnsureComponent(animator);
        }
    }
}