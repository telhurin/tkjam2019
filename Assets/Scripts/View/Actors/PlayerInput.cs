﻿using Eremite.Model;
using UnityEngine;

namespace Eremite.View
{
    public class PlayerInput : MB
    {
        [SerializeField] Player player;

        private void Update()
        {
            if (player.GetSide() == Side.Left)
                ReadLeftInput();

            if (player.GetSide() == Side.Right)
                ReadRightInput();
        }

        private void ReadLeftInput()
        {
            if (Input.GetKeyDown(KeyCode.Q))
                player.ElementPicked(Element.Fire);

            if (Input.GetKeyDown(KeyCode.W))
                player.ElementPicked(Element.Nature);

            if (Input.GetKeyDown(KeyCode.E))
                player.ElementPicked(Element.Water);
        }

        private void ReadRightInput()
        {
            if (Input.GetKeyDown(KeyCode.I))
                player.ElementPicked(Element.Fire);

            if (Input.GetKeyDown(KeyCode.O))
                player.ElementPicked(Element.Nature);

            if (Input.GetKeyDown(KeyCode.P))
                player.ElementPicked(Element.Water);
        }

        private void OnValidate()
        {
            player = EnsureComponent(player);
        }
    }
}
