﻿using Eremite.Model;
using UnityEngine;

namespace Eremite.View
{
    public class Player : MB
    {
        [SerializeField] Side side = Side.None;
        [SerializeField] PlayerInput input;
        [SerializeField] Animator animator;
        [SerializeField] Transform spellAnchor;

        private PlayerState state;
        private SpellView spell;

        public void Start()
        {
            state = PlayersService.GetPlayer(side);
            PlayersService.Register(side, this);
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            GameService.OnPlayersPicked += PlayersPicked;
            GameService.OnClash += OnClash;
            GameService.OnAttacksFinished += OnAttacksFinished;
            GameService.OnNewTurn += OnNewTurn;
            GameService.OnGameFinished += OnGameFinished;
        }

        private void PlayersPicked()
        {
            CreateSpell();
        }

        private void CreateSpell()
        {
            if (GameService.GetCurrentPickFor(side) == Element.None)
                return;

            animator.SetTrigger("Casting");
            spell = SpellsService.GetSpellFor(GameService.GetCurrentPickFor(side));
            spell.SetUp(side, spellAnchor.position, PlayersService.GetPlayerView(side.Opposite()).GetTargetPosition());
        }

        private void OnClash(Side side)
        {
            if (this.side != side)
                spell?.Remove();

            spell = null;
        }

        private void OnAttacksFinished(Side winner)
        {
            if (winner != Side.None && side != winner)
                ShowDamaged(winner);
        }

        private void ShowDamaged(Side winner)
        {
            animator.SetTrigger("Hurt");
            FloatingTextsService.ShowDamage(transform.position, Mathf.RoundToInt(PlayersService.GetTurnCurrentDamage(winner)));
        }

        private void OnNewTurn()
        {
            animator.SetTrigger("Idle");
            input.enabled = true;
        }

        private void OnGameFinished(Side side)
        {
            if (this.side != side)
                Die();
        }

        private void Die()
        {
            FindChild("PlayerUI").SetActive(false);
            animator.SetTrigger("Die");
        }

        public void ElementPicked(Element element)
        {
            input.enabled = false;
            GameService.RegisterMove(side, element);
            Sounds.SpellsCast();
        }

        private void OnValidate()
        {
            input = EnsureComponent(input);
            animator = EnsureComponent(animator);
            spellAnchor = EnsureComponent(spellAnchor, "SpellAnchor");
        }

        public Side GetSide()
        {
            return side;
        }

        public Vector2 GetTargetPosition()
        {
            return new Vector2(transform.position.x, spellAnchor.position.y);
        }
    }
}
