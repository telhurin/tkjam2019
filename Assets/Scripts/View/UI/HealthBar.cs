﻿using UnityEngine;
using UnityEngine.UI;

namespace Eremite.View
{
    public class HealthBar : MB
    {
        [SerializeField] Player player;
        [SerializeField] Slider slider;

        private void Start()
        {
            slider.maxValue = Settings.BaseHealth;
            slider.value = Settings.BaseHealth;
        }

        private void Update()
        {
            slider.value = PlayersService.GetPlayer(player.GetSide()).hp;
        }

        private void OnValidate()
        {
            player = EnsureComponent(player, "", 2);
            slider = EnsureComponent(slider, "Slider");
        }
    }
}
