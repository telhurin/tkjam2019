﻿using Eremite.Model;
using TMPro;
using UnityEngine;

namespace Eremite.View
{
    public class DrawCounter : MB
    {
        [SerializeField] Animator animator;
        [SerializeField] TextMeshProUGUI counterText;

        private void Start()
        {
            Register();
        }

        private void Register()
        {
            GameService.OnAttacksFinished += OnAttacksFinished;
        }

        private void OnAttacksFinished(Side side)
        {
            if(side != Side.None)
            {
                Disable();
                return;
            }

            ShowDraw();
        }

        private void Disable()
        {
            animator.SetTrigger("Idle");
        }

        private void ShowDraw()
        {
            animator.SetTrigger("Show");
            counterText.text = $"x{Mathf.RoundToInt(PlayersService.CalculateDrawFactor())}";
        }

        private void OnValidate()
        {
            animator = EnsureComponent(animator);
            counterText = EnsureComponent(counterText, "Text");
        }
    }
}
