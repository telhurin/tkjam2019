﻿using System;
using Eremite.Model;
using TMPro;
using UnityEngine;
using UniRx;

namespace Eremite.View
{
    public class GameResultPopup : MB
    {
        [SerializeField] TextMeshProUGUI resultText;
        [SerializeField] Animator animator;

        private IDisposable checkInputRoutine;

        private void Start()
        {
            Register();
        }

        private void Register()
        {
            GameService.OnGameFinished += OnGameFinished;
        }

        private void OnGameFinished(Side winner)
        {
            Sounds.GameFinished();
            animator.SetTrigger("Show");
            resultText.text = $"{winner} player won";
            WaitForInput();
        }

        private void WaitForInput()
        {
            checkInputRoutine = RXService.EveryUpdate().Subscribe(CheckInput);
        }

        private void CheckInput(long delta)
        {
            if(Input.anyKeyDown)
            {
                checkInputRoutine.Dispose();
                Controller.ShowMenu();
            }
        }

        private void OnValidate()
        {
            animator = EnsureComponent(animator);
            resultText = EnsureComponent(resultText, "Background/Text");
        }
    }
}
