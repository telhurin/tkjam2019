﻿using System;
using Eremite.Model;
using TMPro;
using UnityEngine;

namespace Eremite.View
{
    public class ResultPresenter : MB
    {
        [SerializeField] Animator animator;
        [SerializeField] TextMeshProUGUI resultText;

        private void Start()
        {
            GameService.OnAttacksFinished += OnAttacksFinished;
            GameService.OnGameFinished += OnGameFinished;
        }

        private void OnAttacksFinished(Side winner)
        {
            ShowResult(winner);
        }

        private void OnGameFinished(Side winner)
        {
            SetActive(false);
        }

        private void ShowResult(Side winner)
        {
            switch (winner)
            {
                case Side.None:
                    ShowDraw();
                    break;
                case Side.Left:
                    ShowLeftWon();
                    break;
                case Side.Right:
                    ShowRightWon();
                    break;
                default:
                    throw new NotImplementedException(winner.ToString());
            }
        }

        private void ShowDraw()
        {
            resultText.text = "DRAW!";
            animator.SetTrigger("Show");
        }

        private void ShowLeftWon()
        {
            resultText.text = "LEFT WINS!";
            animator.SetTrigger("Show");
        }

        private void ShowRightWon()
        {
            resultText.text = "RIGHT WINS!";
            animator.SetTrigger("Show");
        }

        private void OnValidate()
        {
            animator = EnsureComponent(animator);
            resultText = EnsureComponent(resultText, "Text");
        }
    }
}
