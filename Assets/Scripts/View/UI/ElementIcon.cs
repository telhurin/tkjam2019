﻿using Eremite.Model;
using TMPro;
using UnityEngine;

namespace Eremite.View
{
    public class ElementIcon : MB
    {
        [SerializeField] Animator animator;
        [SerializeField] Player player;
        [SerializeField] TextMeshPro textMesh;
        [SerializeField] Element element = Element.None;

        private void Start()
        {
            Register();
            SetText(Settings.BaseAttack);
        }

        private void Register()
        {
            PlayersService.OnElementUpgrade += OnUpgrade;
        }

        private void OnUpgrade(Side side, Element element)
        {
            if (player.GetSide() != side || this.element != element)
                return;

            ShowUpgrade();
        }

        private void ShowUpgrade()
        {
            animator.SetTrigger("Bounce");
            SetText(PlayersService.GetPlayer(player.GetSide()).GetDamage(element));
            FloatingTextsService.ShowText(transform.position, "x2");
        }

        private void SetText(float counter)
        {
            textMesh.text = Mathf.RoundToInt(counter).ToString();
        }

        private void OnValidate()
        {
            animator = EnsureComponent(animator);
            player = EnsureComponent(player, "", 3);
            textMesh = EnsureComponent(textMesh, "Counter");
        }
    }
}
