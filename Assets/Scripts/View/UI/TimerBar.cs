﻿using Eremite.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Eremite.View
{
    public class TimerBar : MB
    {
        [SerializeField] Slider slider;
        [SerializeField] Side side = Side.None;

        private void Start()
        {
            Register();
            SetUpSlider();
        }

        private void SetUpSlider()
        {
            SetActive(true);
            slider.maxValue = Settings.TimeToPick;
            slider.value = Settings.TimeToPick;
        }

        private void Register()
        {
            GameService.OnNewTurn += OnNewTurn;
            GameService.OnPlayerPicked += OnPlayerPicked;
        }

        private void OnPlayerPicked(Side side)
        {
            if (this.side != side)
                return;

            SetActive(false);
        }

        private void OnNewTurn()
        {
            SetUpSlider();
        }

        private void Update()
        {
            slider.value = GameService.GetTimeLeft();
        }

        private void OnValidate()
        {
            slider = EnsureComponent(slider);
        }
    }
}
