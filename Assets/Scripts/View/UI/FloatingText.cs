﻿using System;
using TMPro;
using UnityEngine;
using UniRx;

namespace Eremite.View
{
    public class FloatingText : MB
    {
        public float y;
        public float floatTime;
        public TextMeshPro textMesh;

        private float startTime;
        private float journeyLength;
        private IDisposable updateRoutine;

        private Vector2 from;
        private Vector2 to;

        public void Show(string text)
        {
            SetUpJourney();
            SetUpRoutine();
            SetUpText(text);
            UpdatePosition(0);
            SetActive(true);
        }

        private void SetUpRoutine()
        {
            updateRoutine = RXService.EveryUpdate().Subscribe(UpdatePosition);
        }

        private void SetUpJourney()
        {
            from = transform.position;
            to = from + new Vector2(0, y);
            startTime = Time.time;
            journeyLength = floatTime;
        }

        private void SetUpText(string text)
        {
            textMesh.text = text;
        }

        private void UpdatePosition(long delta)
        {
            float fracJourney = Mathf.Clamp01((Time.time - startTime) / journeyLength);

            if (Mathf.Approximately(fracJourney, 1))
            {
                Finished();
                return;
            }

            transform.position = Vector3.Lerp(from, to, Easing.Sinusoidal.InOut(fracJourney));
        }

        public void Finished()
        {
            updateRoutine.Dispose();
            SetActive(false);
            Destroy(gameObject);
        }
    }
}