﻿using Eremite.Model;
using UnityEngine;

namespace Eremite.View
{
    public class MenuPlayer : MB
    {
        private const string ready = "Ready";

        [SerializeField] Side side = Side.None;
        [SerializeField] Animator fire;
        [SerializeField] Animator water;
        [SerializeField] Animator nature;
        [SerializeField] Animator mainAnimator;

        private void Update()
        {
            if (side == Side.Left)
                ReadLeftInput();

            if (side == Side.Right)
                ReadRightInput();

            mainAnimator.SetBool(ready, IsReady());
        }

        private void ReadLeftInput()
        {
            if (Input.GetKeyDown(KeyCode.Q))
                SetFor(fire);

            if (Input.GetKeyDown(KeyCode.W))
                SetFor(nature);

            if (Input.GetKeyDown(KeyCode.E))
                SetFor(water);
        }

        private void SetFor(Animator animator)
        {
            if (animator.GetBool(ready))
                return;

            Sounds.Click();
            animator.SetBool(ready, true);
        }

        private void ReadRightInput()
        {
            if (Input.GetKeyDown(KeyCode.I))
                SetFor(fire);

            if (Input.GetKeyDown(KeyCode.O))
                SetFor(nature);

            if (Input.GetKeyDown(KeyCode.P))
                SetFor(water);
        }

        public bool IsReady()
        {
            return fire.GetBool(ready) && water.GetBool(ready) && nature.GetBool(ready);
        }

        private void OnValidate()
        {
            mainAnimator = EnsureComponent(mainAnimator);
            fire = EnsureComponent(fire, "Fire");
            water = EnsureComponent(water, "Water");
            nature = EnsureComponent(nature, "Nature");
        }
    }
}
