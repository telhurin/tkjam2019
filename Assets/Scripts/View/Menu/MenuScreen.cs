﻿using UnityEngine;

namespace Eremite.View
{
    public class MenuScreen : MB
    {
        [SerializeField] MenuPlayer left;
        [SerializeField] MenuPlayer right;

        private void Update()
        {
            if (left.IsReady() && right.IsReady())
                LoadGame();
        }

        private void LoadGame()
        {
            Controller.ShowGame();
        }

        private void OnValidate()
        {
            left = EnsureComponent(left, "Left");
            right = EnsureComponent(right, "Right");
        }
    }
}
