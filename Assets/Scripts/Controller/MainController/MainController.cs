﻿using Eremite.Model;
using Eremite.Services;
using Eremite.View;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Eremite.Controller
{
    public interface IController
    {
        GameServices Services { get; }
        Settings Settings { get; }
        SoundReferences Sounds { get; }
        SoundsManager SoundsManager { get; }
        Camera MainCamera { get; }

        void ShowMenu();
        void ShowGame();
    }

    public class MainController : MonoBehaviour, IController
    {
        public static IController Instance { get; set; }

        public GameServices Services { get; private set; }

        public Camera MainCamera { get { return refs.mainCamera; } }
        public Settings Settings { get { return refs.settings; } }
        public SoundReferences Sounds { get { return refs.sounds; } }
        public SoundsManager SoundsManager { get { return refs.soundsManager; } }

        protected ControllerReferences refs;

        public void Awake()
        {
            if (!EnforceSingleton())
                return;

            InitReferences();
            InitServices();
            InitApplication();
            DontDestroyOnLoad(gameObject);
        }

        private bool EnforceSingleton()
        {
            if (Instance != null && !Instance.Equals(this))
            {
                Destroy(gameObject);
                return false;
            }

            Instance = this;
            return true;
        }

        private void InitReferences()
        {
            refs = GetComponent<ControllerReferences>();
        }

        private void InitApplication()
        {
            Application.targetFrameRate = 60;
        }

        private void InitServices()
        {
            Services = new GameServices();
            Services.CreateServices(this);
            Services.Init(OnServicesReady);

            if (SceneManager.GetActiveScene().buildIndex == 1)
                Services.OnGameStart();
        }

        private void OnServicesReady()
        {
            Log.Info("Loading completed");
        }

        public void ShowGame()
        {
            SceneManager.LoadScene(1);
            Services.OnGameStart();
        }

        public void ShowMenu()
        {
            Services.Destroy();
            Services = null;
            SceneManager.LoadScene(0);
            InitServices();
        }
    }
}