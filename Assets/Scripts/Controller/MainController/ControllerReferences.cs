﻿using Eremite.Model;
using Eremite.View;
using UnityEngine;

namespace Eremite.Controller
{
    public class ControllerReferences : MonoBehaviour
    {
        public Camera mainCamera;
        public Settings settings;
        public SoundReferences sounds;
        public SoundsManager soundsManager;
    }
}
