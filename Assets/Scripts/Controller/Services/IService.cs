﻿using System;
using System.Collections.Generic;

namespace Eremite.Services
{
    public interface IService
    {
        bool Loaded { get; }

        void Load(Action callback);
        void AddInitCallback(Action action);
        void RemoveInitCallback(Action action);
        void OnDestroy();
    }

    public abstract class Service : Serviceable, IService
    {
        public bool Loaded { get; private set; }

        private bool loadingStarted;
        private readonly List<Action> loadingCallbacks = new List<Action>();
        private IService[] blockingServices;

        protected virtual List<IService> GetLoadingBlockers()
        {
            return new List<IService>();
        }

        protected virtual void OnLoading(Action onLoaded)
        {
            onLoaded();
        }

        public virtual void OnDestroy()
        {
        }

        public virtual void OnGameStart()
        {
        }

        public void AddInitCallback(Action action)
        {
            if (Loaded && action != null)
            {
                action();
                return;
            }

            loadingCallbacks.Add(action);
        }

        public void RemoveInitCallback(Action action)
        {
            loadingCallbacks.Remove(action);
        }

        protected virtual int CountBlockers()
        {
            if (blockingServices == null)
                blockingServices = GetLoadingBlockers().ToArray();

            int blocks = 0;
            for (int i = 0; i < blockingServices.Length; i++)
                if (!blockingServices[i].Loaded)
                    blocks++;

            return blocks;
        }

        public void Load(Action callback)
        {
            if (Loaded || loadingStarted)
                throw new Exception("Init called more then once on " + GetType().Name);

            AddInitCallback(callback);

            if (CountBlockers() == 0)
            {
                Load();
                return;
            }

            var blocking = GetLoadingBlockers();
            for (int i = 0; i < blocking.Count; i++)
                blocking[i].AddInitCallback(BlockerLoaded);
        }

        private void BlockerLoaded()
        {
            if (CountBlockers() == 0)
                Load();
        }

        private void Load()
        {
            loadingStarted = true;

            try
            {
                InitThis();
            }
            catch(Exception e)
            {
                Log.Exception(e);
            }
        }

        private void InitThis()
        {
            var time = Services.TimeService.DateTime;
            OnLoading(() =>
            {
                Loaded = true;
                //Log.Info($"SERVICE LOADED: {GetType().Name} in + {(Services.TimeService.DateTime - time)}");
                List<Action> actions = new List<Action>(loadingCallbacks);
                loadingCallbacks.Clear();
                actions.ForEach(action => action());
            });
        }
    }
}