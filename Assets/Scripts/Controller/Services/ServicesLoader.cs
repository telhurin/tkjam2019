﻿using System.Collections.Generic;
using System;

namespace Eremite.Services
{
    public interface IServicesLoader
    {
    }

    public class ServicesLoader : IServicesLoader
    {
        private readonly List<Service> toLoad = new List<Service>();
        private readonly Action onLoadingCompleted;

        private int loadedServices = 0;


        public ServicesLoader(List<Service> toLoad, Action onLoadingCompleted)
        {
            this.toLoad = toLoad;
            this.onLoadingCompleted = onLoadingCompleted;
        }

        public void Load()
        {
            toLoad.ForEach(s => s.Load(() => ServiceLoaded(toLoad.Count)));
        }

        protected void ServiceLoaded(int servicesToLoad)
        {
            loadedServices++;
            if (loadedServices == servicesToLoad)
                LoadingCompleted();
        }

        protected void LoadingCompleted()
        {
            onLoadingCompleted();
        }
    }
}

