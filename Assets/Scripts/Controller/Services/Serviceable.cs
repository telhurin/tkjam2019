﻿using Eremite.Controller;
using Eremite.Model;
using Eremite.View;

namespace Eremite.Services
{
    public abstract class Serviceable 
    {
        protected IController Controller { get { return MainController.Instance; } }
        protected GameServices Services { get { return Controller.Services; } }
        protected Settings Settings { get { return Controller.Settings; } }
        protected IRXService RXService { get { return Controller.Services.RXService; } }
        protected PlayersService PlayersService { get { return Controller.Services.PlayersService; } }
        protected GameService GameService { get { return Controller.Services.GameService; } }
        protected SoundReferences Sounds { get { return Controller.Sounds; } }
    }
}