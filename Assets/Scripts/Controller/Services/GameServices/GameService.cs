﻿using Eremite.Model;
using System;
using UniRx;
using UnityEngine;

namespace Eremite.Services
{
    public class GameService : Service
    {
        public Action OnPlayersPicked { get; set; }
        public Action<Side> OnPlayerPicked { get; set; }
        public Action<Side> OnClash { get; set; }
        public Action<Side> OnAttacksFinished { get; set; }

        public Action OnNewTurn { get; set; }
        public Action<Side> OnGameFinished { get; set; }

        public int Turn { get => game.turns.Count; }
        public float Draws { get => game.draws; }

        private IDisposable travelRoutine;
        private IDisposable timerRoutine;
        private GameState game;

        public override void OnGameStart()
        {
            game = new GameState();
            StartTimer();
        }

        private void StartTimer()
        {
            timerRoutine = RXService.Timer(Settings.TimeToPick).Subscribe(TimeOut);
        }

        private void TimeOut(long delta)
        {
            if (!HasPlayerPicked(Side.Right))
                RegisterMove(Side.Right, Element.None);

            if (!HasPlayerPicked(Side.Left))
                RegisterMove(Side.Left, Element.None);
        }

        public void RegisterMove(Side side, Element element)
        {
            if (HasPlayerPicked(side))
                throw new Exception($"Player {side} already picked spell in turn {Turn}");

            SetMoveFor(side, element);
            OnPlayerPicked?.Invoke(side);

            if (AreBothPlayersReady())
                PlayersPicked();
        }

        private bool HasPlayerPicked(Side side)
        {
            return game.GetLastTurn().picks.ContainsKey(side);
        }

        private void SetMoveFor(Side side, Element element)
        {
            game.GetLastTurn().picks[side] = element;
        }

        private bool AreBothPlayersReady()
        {
            return game.GetLastTurn().picks.Count == 2;
        }

        private void PlayersPicked()
        {
            timerRoutine.Dispose();

            OnPlayersPicked();
            RXService.Timer(Settings.AttacksClashTime).Subscribe(SpellsClashed);
            travelRoutine = RXService.Timer(Settings.AttacksTravelTime).Subscribe(AttacksFinished);
        }

        private void SpellsClashed(long delta)
        {
            Side winner = GetTurnWinner();
            Element winnerElement = winner != Side.None ? game.GetLastTurn().picks[winner] : Element.None;

            if (winner == Side.None)
                Draw();

            Sounds.PlayElementSound(winnerElement);
            OnClash(winner);
        }

        private void Draw()
        {
            UnityEngine.Debug.Log($"DRAW: {Draws}");
            game.draws++;
            travelRoutine.Dispose();
            OnAttacksFinished(Side.None);
            NewTurn();
        }

        private void AttacksFinished(long delta)
        {
            Side winner = GetTurnWinner();
            UnityEngine.Debug.Log($"ATTACKS FINISHED. WINNER: {winner}");

            PlayersService.DamageLooser(winner, game.GetLastTurn().picks[winner]);
            OnAttacksFinished(winner);

            game.draws = 0;
            PlayersService.RewardWinner(winner, game.GetLastTurn().picks[winner]);
            FinishTurn();
        }

        private void NewTurn()
        {
            UnityEngine.Debug.Log($"NEW TURN: {Turn}");
            game.turns.Add(new TurnState());
            StartTimer();
            OnNewTurn();
        }

        private void FinishTurn()
        {
            if (PlayersService.GetWinner() != Side.None)
                OnGameFinished(PlayersService.GetWinner());
            else
                ScheduleNewTurn();
        }

        private void ScheduleNewTurn()
        {
            RXService.Timer(Settings.TimeBetweenTurns).Subscribe(l => NewTurn());
        }

        private Side GetTurnWinner()
        {
            Element left = game.GetLastTurn().picks[Side.Left];
            Element right = game.GetLastTurn().picks[Side.Right];

            if (left == right)
                return Side.None;

            if (left == Element.None)
                return Side.Right;

            if (right == Element.None)
                return Side.Left;

            if (left == Element.Water)
                return right == Element.Nature ? Side.Right : Side.Left;

            if (left == Element.Fire)
                return right == Element.Water ? Side.Right : Side.Left;

            if (left == Element.Nature)
                return right == Element.Fire ? Side.Right : Side.Left;

            throw new Exception($"CANT FIND WINNER L: {left} R: {right}");
        }

        public Element GetCurrentPickFor(Side side)
        {
            if (!game.GetLastTurn().picks.ContainsKey(side))
                return Element.None;

            return game.GetLastTurn().picks[side];
        }

        public float GetTimeLeft()
        {
            return Settings.TimeToPick - (Time.time - game.GetLastTurn().startTime);
        }

        public override void OnDestroy()
        {
            OnPlayersPicked = null;
            OnPlayerPicked = null;
            OnClash = null;
            OnAttacksFinished = null;

            OnNewTurn = null;
            OnGameFinished = null;

            timerRoutine?.Dispose();
            travelRoutine?.Dispose();
        }
    }
}
