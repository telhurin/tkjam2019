﻿using Eremite.View;
using System;
using UnityEngine;

namespace Eremite.Services
{
    public class FloatingTextsService : Service
    {
        private FloatingText damagePrefab;
        private FloatingText textPrefab;

        protected override void OnLoading(Action onComplete)
        {
            damagePrefab = Resources.Load<FloatingText>("Prefabs/Effects/Damage");
            if (!damagePrefab)
                throw new Exception("Cant find damage prefab");

            textPrefab = Resources.Load<FloatingText>("Prefabs/Effects/Text");
            if (!damagePrefab)
                throw new Exception("Cant find text prefab");

            onComplete();
        }

        public void ShowDamage(Vector2 position, int damage)
        {
            var effect = GameObject.Instantiate(damagePrefab);
            effect.transform.position = position;
            effect.Show(damage.ToString());
        }

        public void ShowText(Vector2 position, string text)
        {
            var effect = GameObject.Instantiate(textPrefab);
            effect.transform.position = position;
            effect.Show(text);
        }
    }
}