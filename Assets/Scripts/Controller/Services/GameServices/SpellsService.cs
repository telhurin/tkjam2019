﻿using Eremite.Model;
using Eremite.View;
using System;
using UnityEngine;

namespace Eremite.Services
{
    public class SpellsService : Service
    {
        private SpellView fire;
        private SpellView water;
        private SpellView nature;

        protected override void OnLoading(Action onLoaded)
        {
            LoadPrefabs();
            onLoaded();
        }

        private void LoadPrefabs()
        {
            fire = Resources.Load<SpellView>("Prefabs/Spells/Fire");
            water = Resources.Load<SpellView>("Prefabs/Spells/Water");
            nature = Resources.Load<SpellView>("Prefabs/Spells/Nature");
        }

        public SpellView GetSpellFor(Element element)
        {
            return CreateSpell(GetPrefabFor(element));
        }

        private SpellView GetPrefabFor(Element element)
        {
            switch (element)
            {
                case Element.Fire:
                    return fire;
                case Element.Water:
                    return water;
                case Element.Nature:
                    return nature;
                default:
                    throw new NotImplementedException(element.ToString());
            }
        }

        private SpellView CreateSpell(SpellView prefab)
        {
            return GameObject.Instantiate(prefab);
        }
    }
}
