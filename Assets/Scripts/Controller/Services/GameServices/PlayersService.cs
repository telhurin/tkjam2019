﻿using System;
using Eremite.Model;
using Eremite.View;

namespace Eremite.Services
{
    public class PlayersService : Service
    {
        public Action<Side, Element> OnElementUpgrade { get; set; }

        public PlayerState Left { get; private set; }
        public PlayerState Right { get; private set; }

        public Player LeftView { get; private set; }
        public Player RightView { get; private set; }

        protected override void OnLoading(Action onLoaded)
        {
            Left = CreatePlayer(Side.Left);
            Right = CreatePlayer(Side.Right);
            onLoaded();
        }

        public void Register(Side side, Player player)
        {
            if (side == Side.Left)
                LeftView = player;

            if (side == Side.Right)
                RightView = player;
        }

        private PlayerState CreatePlayer(Side side)
        {
            return new PlayerState
            {
                side = side,
                hp = Settings.BaseHealth,
                fireAttack = Settings.BaseAttack,
                waterAttack = Settings.BaseAttack,
                natureAttack = Settings.BaseAttack
            };
        }

        public PlayerState GetPlayer(Side side)
        {
            return side == Side.Left ? Left : Right;
        }

        public void DamageLooser(Side winner, Element element)
        {
            GetPlayer(winner.Opposite()).hp -= GetTurnCurrentDamage(winner);
        }

        public void RewardWinner(Side winner, Element element)
        {
            GetPlayer(winner).AddDamage(element, Settings.AttackWinMultiplayer);
            OnElementUpgrade?.Invoke(winner, element);
        }

        public Player GetPlayerView(Side side)
        {
            return side == Side.Left ? LeftView : RightView;
        }

        public Side GetWinner()
        {
            if (Left.hp <= 0)
                return Side.Right;

            if (Right.hp <= 0)
                return Side.Left;

            return Side.None;
        }

        public float GetTurnCurrentDamage(Side side)
        {
            return GetPlayer(side).GetDamage(GameService.GetCurrentPickFor(side)) * CalculateDrawFactor();
        }

        public float CalculateDrawFactor()
        {
            if (GameService.Draws == 0)
                return 1;

            return GameService.Draws * Settings.DrawAttackMultiplayer;
        }

        public override void OnDestroy()
        {
            OnElementUpgrade = null;
        }
    }
}
