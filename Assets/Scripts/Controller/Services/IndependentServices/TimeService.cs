﻿using System;

namespace Eremite.Services
{
    public interface ITimeService : IService
    {
        float DeltaTime { get; }

        DateTime DateTime { get; }
        long Time { get; }
        long DateTimeToMs(DateTime d);
        DateTime MsToDateTime(long ms);
    }

    public class TimeService : Service, ITimeService
    {
        private readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public DateTime DateTime
        {
            get
            {
                return DateTime.UtcNow;
            }
        }

        public long Time
        {
            get
            {
                return Convert.ToInt64((DateTime.UtcNow - epoch).TotalMilliseconds);
            }
        }

        public float DeltaTime
        {
            get
            {
                return UnityEngine.Time.deltaTime;
            }
        }

        public long DateTimeToMs(DateTime d)
        {
            return Convert.ToInt64((d - epoch).TotalMilliseconds);
        }

        public DateTime MsToDateTime(long ms)
        {
            return epoch.AddMilliseconds(ms);
        }
    }
}
