﻿using System;
using System.Collections;
using UniRx;

namespace Eremite.Services
{
    public interface IRXService : IService
    {
        IObservable<long> EveryUpdate();
        IObservable<T> FromCoroutine<T>(Func<IObserver<T>, IEnumerator> coroutine);
        IObservable<long> TimerFrame(int frames);
        IObservable<long> Timer(float seconds);
        IObservable<long> Interval(float seconds);
    }

    public class RXService : Service, IRXService
    {
        public IObservable<long> EveryUpdate()
        {
            return Observable.EveryUpdate();
        }

        public IObservable<T> FromCoroutine<T>(Func<IObserver<T>, IEnumerator> coroutine)
        {
            return Observable.FromCoroutine<T>(coroutine);
        }

        public IObservable<long> TimerFrame(int frames)
        {
            return Observable.TimerFrame(frames);
        }

        public IObservable<long> Timer(float seconds)
        {
            return Observable.Timer(TimeSpan.FromSeconds(seconds));
        }

        public IObservable<long> Interval(float seconds)
        {
            return Observable.Interval(TimeSpan.FromSeconds(seconds));
        }
    }
}