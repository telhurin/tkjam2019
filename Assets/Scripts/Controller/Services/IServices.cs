﻿using Eremite.Controller;
using System;
using System.Collections.Generic;

namespace Eremite.Services
{
    public class GameServices
    {
        public IController Controller { get; private set; }

        public RXService RXService { get; private set; }
        public TimeService TimeService { get; private set; }
        public PlayersService PlayersService { get; private set; }
        public GameService GameService { get; private set; }
        public FloatingTextsService FloatingTextsService { get; private set; }
        public SpellsService SpellsService { get; private set; }

        private readonly List<Service> allServices = new List<Service>();

        public void CreateServices(IController controller)
        {
            Controller = controller;

            RXService = new RXService();
            allServices.Add(RXService);

            TimeService = new TimeService();
            allServices.Add(TimeService);

            PlayersService = new PlayersService();
            allServices.Add(PlayersService);

            GameService = new GameService();
            allServices.Add(GameService);

            FloatingTextsService = new FloatingTextsService();
            allServices.Add(FloatingTextsService);

            SpellsService = new SpellsService();
            allServices.Add(SpellsService);
        }

        public void Init(Action onComplete)
        {
            new ServicesLoader(allServices, onComplete).Load();
        }

        public void Destroy()
        {
            allServices.ForEach(s => s.OnDestroy());
        }

        public void OnGameStart()
        {
            allServices.ForEach(s => s.OnGameStart());
        }
    }
}