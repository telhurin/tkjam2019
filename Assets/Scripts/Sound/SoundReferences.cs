﻿using Eremite.Model;
using UnityEngine;

namespace Eremite.View
{
    [CreateAssetMenu(menuName = "Eremite/Sound Refs")]
    public class SoundReferences : SO
    {
        [SerializeField] AudioClip button = null;
        [SerializeField] AudioClip cast = null;

        [SerializeField] AudioClip fire = null;
        [SerializeField] AudioClip water = null;
        [SerializeField] AudioClip nature = null;
        [SerializeField] AudioClip draw = null;

        [SerializeField] AudioClip gameFinished = null; 

        public void Click()
        {
            SoundsManager.PlaySoundEffect(button);
        }

        public void SpellsCast()
        {
            SoundsManager.PlaySoundEffect(cast);
        }

        public void PlayElementSound(Element element)
        {
            switch (element)
            {
                case Element.None:
                    SoundsManager.PlaySoundEffect(draw);
                    break;
                case Element.Fire:
                    SoundsManager.PlaySoundEffect(fire);
                    break;
                case Element.Water:
                    SoundsManager.PlaySoundEffect(water);
                    break;
                case Element.Nature:
                    SoundsManager.PlaySoundEffect(nature);
                    break;
            }
        }

        public void GameFinished()
        {
            SoundsManager.PlaySoundEffect(gameFinished);
        }
    }
}