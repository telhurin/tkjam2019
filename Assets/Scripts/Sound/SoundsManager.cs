﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eremite.View
{
    public class SoundsManager : MB
    {
        [SerializeField] AudioSource buttonAudioSource;

        private List<AudioSource> effectsAudioSources = new List<AudioSource>();

        public void PlayButtonSound(AudioClip clip)
        {
            buttonAudioSource.clip = clip;
            buttonAudioSource.Play();
        }

        public void PlaySoundEffect(AudioClip audioClip)
        {
            if (audioClip == null)
                return;

            AudioSource source = GetEffectAudioSource();
            source.clip = audioClip;
            source.Play();
            StartCoroutine(Disable(source, audioClip.length));
        }

        private AudioSource GetEffectAudioSource()
        {
            var source = effectsAudioSources.Find(e => !e.enabled);
            if (source != null)
            {
                source.enabled = true;
                return source;
            }

            source = CreateNewAudioSource();
            effectsAudioSources.Add(source);
            return source;
        }

        private AudioSource CreateNewAudioSource()
        {
            var source = gameObject.AddComponent<AudioSource>();
            source.playOnAwake = false;
            return source;
        }

        private IEnumerator Disable(AudioSource source, float length)
        {
            yield return new WaitForSecondsRealtime(length);
            source.enabled = false;
        }

        private void OnValidate()
        {
            buttonAudioSource = EnsureComponent(buttonAudioSource);
        }
    }
}
